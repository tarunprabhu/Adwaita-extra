#!/usr/bin/env sh

usage="install.sh [-hv] <DEST>"

help="Installs the icons to either a user or a system directory. Installing to a
system directory may require superuser privileges.

    ${usage}

OPTIONS

    -h      Print this help message.

    -v      Enable verbose mode.

ARGUMENTS

    DEST    Whether to install to a user or a system directory. Must be one
            of 'user' or 'system'.
"

verbose=""
while getopts hv opt; do
    case "${opt}" in
        h)
            echo "${help}"
            exit
            ;;
        v)
            verbose=1
            ;;
        *)
            # An error message will already have been printed
            echo "${usage}"
            exit
            ;;
    esac
done
shift $((OPTIND - 1))

dest=$1
if [ -z "${dest}" ]; then
    echo "Missing destination"
    echo ""
    echo "${usage}"
    exit 1
elif [ "${dest}" != "system" ] && [ "${dest}" != "user" ]; then
    echo "Invalid destination: '${dest}'"
    echo
    echo "${usage}"
    exit 1
fi

base=""
if [ "${dest}" = "system" ]; then
    base="/usr/share"
elif [ -z "${XDG_DATA_HOME}" ]; then
    base="${HOME}/.local/share"
else
    base="${XDG_DATA_HOME}"
fi

theme="hicolor"
icondir="${base}/icons/${theme}"
if [ ! -d "${icondir}" ]; then
    [ -n "${verbose}" ] && echo "Creating icon directory"
    mkdir -p "${icondir}"
fi

root=$(dirname $(realpath "$0"))
files=$(find . -type f)
for file in ${files}; do
    filename=$(basename "${file}")
    if [ "${filename}" = "install.sh" ]; then
        continue
    fi

    srcdir="$(realpath $(dirname ${file}))"
    reldir=""
    if [ "${srcdir}" != "${root}" ]; then
        reldir=${srcdir#"${root}/"}
    fi
    destdir="${icondir}/${reldir}"

    if [ ! -d "${destdir}" ]; then
        [ -n "${verbose}" ] && echo "Creating directory: ${destdir}"
        mkdir -p "${destdir}"
    fi

    [ -n "${verbose}" ] && echo "Copying ${filename} to ${destdir}/"
    cp "${srcdir}/${filename}" "${destdir}/${filename}"
done

[ -n "${verbose}" ] && echo "Updating icon cache"
gtk-update-icon-cache "${icondir}"
