# Some symbolic icons

These are some icons for the panel and other things. Some are intended to fill
in the gaps in Adwaita. 

## Installation

Use the installation script `install.sh`. 

```
$ install.sh <user | system>
```

If 'system' is specified, the icons will be installed to the `hicolor` theme in
the system icon directory (typically `/usr/share/icons/hicolor`). This will 
usually require superuser permissions.

If 'user' is specified, the icons will be installed to the `hicolor` theme in
`$XDG_DATA_HOME/icons/hicolor`. If `$XDG_DATA_HOME` is not set, the icons will
be installed in `$HOME/.local/share/icons/hicolor`.
